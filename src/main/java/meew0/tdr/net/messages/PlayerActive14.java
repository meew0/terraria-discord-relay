package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;

import java.io.IOException;

/**
 * Created by meew0 on 20/05/2020.
 */
public class PlayerActive14 extends ServerToClientOnlyPayload {
    private int playerId;
    private boolean active;

    public PlayerActive14() {
    }

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        this.playerId = is.read();
        this.active = (is.read() == 1);
    }

    public int getPlayerId() {
        return playerId;
    }

    public boolean isActive() {
        return active;
    }
}
