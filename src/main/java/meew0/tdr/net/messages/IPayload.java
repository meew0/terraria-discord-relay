package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public interface IPayload {
    void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException;

    void writeTo(LittleEndianDataOutputStream os) throws IOException;

    int getLengthOfPayload();
}
