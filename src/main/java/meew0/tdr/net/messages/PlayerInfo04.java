package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import meew0.tdr.net.NetUtil;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class PlayerInfo04 implements IPayload {
    private static final int DIFFICULTY = 0;

    private int playerId;
    private String name;

    public PlayerInfo04(int playerId, String name) {
        this.playerId = playerId;
        this.name = name;
    }

    public PlayerInfo04() {

    }

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        this.playerId = is.read();
        is.skip(2);
        this.name = NetUtil.readString(is);
        is.skip(4 + 3 * 7 + 1);
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.write(playerId);
        os.write(new byte[2]); // Skin variant, hair
        NetUtil.writeString(os, name);
        os.write(new byte[4 + 3 * 7]); // Styles
        os.write(DIFFICULTY);
    }

    @Override
    public int getLengthOfPayload() {
        return 1 + 2 + NetUtil.getStringLength(name) + 4 + 3 * 7 + 1;
    }

    public int getPlayerId() {
        return playerId;
    }

    public String getName() {
        return name;
    }
}
