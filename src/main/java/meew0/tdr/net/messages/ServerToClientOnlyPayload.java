package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataOutputStream;

/**
 * Created by meew0 on 19/05/2020.
 */
public abstract class ServerToClientOnlyPayload implements IPayload {
    @Override
    public void writeTo(LittleEndianDataOutputStream os) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getLengthOfPayload() {
        throw new UnsupportedOperationException();
    }
}
