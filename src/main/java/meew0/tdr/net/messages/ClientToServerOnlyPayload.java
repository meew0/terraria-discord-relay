package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;

/**
 * Created by meew0 on 19/05/2020.
 */
public abstract class ClientToServerOnlyPayload implements IPayload {
    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) {
        throw new UnsupportedOperationException();
    }
}
