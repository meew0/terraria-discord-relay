package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class WorldInfo07 extends ServerToClientOnlyPayload {
    private int spawnX, spawnY;

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        is.skip(10);
        spawnX = is.readShort();
        spawnY = is.readShort();
        is.skip(lengthOfPayload - 14);
    }

    public int getSpawnX() {
        return spawnX;
    }

    public int getSpawnY() {
        return spawnY;
    }
}
