package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import meew0.tdr.net.NetUtil;

import java.io.IOException;
import java.util.EnumSet;

/**
 * Created by meew0 on 20/05/2020.
 */
public class UpdatePlayer13 implements IPayload {
    public enum Control {
        ControlUp, ControlDown, ControlLeft, ControlRight, ControlJump, ControlUseItem, Direction
    }

    public enum Pulley {
        PulleyEnabled, Direction, UpdateVelocity, VortexStealthActive, GravityDirection, ShieldRaised
    }

    public enum Misc {
        HoveringUp, VoidVaultEnabled, Sitting, DownedDD2Event,
        IsPettingAnimal, IsPettingSmallAnimal, UsedPotionofReturn, HoveringDown
    }

    public enum SleepingInfo {
        IsSleeping
    }

    private int playerId, selectedItem;
    private EnumSet<Control> control = EnumSet.noneOf(Control.class);
    private EnumSet<Pulley> pulley = EnumSet.noneOf(Pulley.class);
    private EnumSet<Misc> misc = EnumSet.noneOf(Misc.class);
    private EnumSet<SleepingInfo> sleepingInfo = EnumSet.noneOf(SleepingInfo.class);
    private float positionX, positionY;
    private float velocityX, velocityY;
    private float originalPositionX, originalPositionY, homePositionX, homePositionY;

    public UpdatePlayer13() {
    }

    public UpdatePlayer13(int playerId, int selectedItem, EnumSet<Control> control, EnumSet<Pulley> pulley, EnumSet<Misc> misc, EnumSet<SleepingInfo> sleepingInfo, float positionX, float positionY) {
        this.playerId = playerId;
        this.selectedItem = selectedItem;
        this.control = control;
        this.pulley = pulley;
        this.misc = misc;
        this.sleepingInfo = sleepingInfo;
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public UpdatePlayer13(int playerId, int selectedItem, EnumSet<Control> control, EnumSet<Pulley> pulley, EnumSet<Misc> misc, EnumSet<SleepingInfo> sleepingInfo, float positionX, float positionY, float velocityX, float velocityY) {
        this(playerId, selectedItem, control, pulley, misc, sleepingInfo, positionX, positionY);
        this.velocityX = velocityX;
        this.velocityY = velocityY;
    }

    public UpdatePlayer13(int playerId, int selectedItem, EnumSet<Control> control, EnumSet<Pulley> pulley, EnumSet<Misc> misc, EnumSet<SleepingInfo> sleepingInfo, float positionX, float positionY, float originalPositionX, float originalPositionY, float homePositionX, float homePositionY) {
        this(playerId, selectedItem, control, pulley, misc, sleepingInfo, positionX, positionY);
        this.originalPositionX = originalPositionX;
        this.originalPositionY = originalPositionY;
        this.homePositionX = homePositionX;
        this.homePositionY = homePositionY;
    }

    public UpdatePlayer13(int playerId, int selectedItem, EnumSet<Control> control, EnumSet<Pulley> pulley, EnumSet<Misc> misc, EnumSet<SleepingInfo> sleepingInfo, float positionX, float positionY, float velocityX, float velocityY, float originalPositionX, float originalPositionY, float homePositionX, float homePositionY) {
        this(playerId, selectedItem, control, pulley, misc, sleepingInfo, positionX, positionY);
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.originalPositionX = originalPositionX;
        this.originalPositionY = originalPositionY;
        this.homePositionX = homePositionX;
        this.homePositionY = homePositionY;
    }

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        playerId = is.read();
        NetUtil.readEnumSet(is, control, Control.values());
        NetUtil.readEnumSet(is, pulley, Pulley.values());
        NetUtil.readEnumSet(is, misc, Misc.values());
        NetUtil.readEnumSet(is, sleepingInfo, SleepingInfo.values());
        selectedItem = is.read();

        positionX = is.readFloat();
        positionY = is.readFloat();

        if (pulley.contains(Pulley.UpdateVelocity)) {
            velocityX = is.readFloat();
            velocityY = is.readFloat();
        }

        if (misc.contains(Misc.UsedPotionofReturn)) {
            originalPositionX = is.readFloat();
            originalPositionY = is.readFloat();
            homePositionX = is.readFloat();
            homePositionY = is.readFloat();
        }
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.write(playerId);
        NetUtil.writeEnumSet(os, control);
        NetUtil.writeEnumSet(os, pulley);
        NetUtil.writeEnumSet(os, misc);
        NetUtil.writeEnumSet(os, sleepingInfo);
        os.write(selectedItem);
        os.writeFloat(positionX);
        os.writeFloat(positionY);

        if (pulley.contains(Pulley.UpdateVelocity)) {
            os.writeFloat(velocityX);
            os.writeFloat(velocityY);
        }

        if (misc.contains(Misc.UsedPotionofReturn)) {
            os.writeFloat(originalPositionX);
            os.writeFloat(originalPositionY);
            os.writeFloat(homePositionX);
            os.writeFloat(homePositionY);
        }
    }

    @Override
    public int getLengthOfPayload() {
        return 14
                + (pulley.contains(Pulley.UpdateVelocity) ? 8 : 0)
                + (misc.contains(Misc.UsedPotionofReturn) ? 16 : 0);
    }

    public boolean isControlBitSet(Control v) {
        return control.contains(v);
    }

    public boolean isPulleyBitSet(Pulley v) {
        return pulley.contains(v);
    }

    public boolean isMiscBitSet(Misc v) {
        return misc.contains(v);
    }

    public boolean isSleepingInfoBitSet(SleepingInfo v) {
        return sleepingInfo.contains(v);
    }

    public int getPlayerId() {
        return playerId;
    }

    public int getSelectedItem() {
        return selectedItem;
    }

    public float getPositionX() {
        return positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public float getVelocityX() {
        return velocityX;
    }

    public float getVelocityY() {
        return velocityY;
    }

    public float getOriginalPositionX() {
        return originalPositionX;
    }

    public float getOriginalPositionY() {
        return originalPositionY;
    }

    public float getHomePositionX() {
        return homePositionX;
    }

    public float getHomePositionY() {
        return homePositionY;
    }
}
