package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class GenericPayload implements IPayload {
    private byte[] payload;

    public byte[] getPayload() {
        return payload;
    }

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        payload = is.readNBytes(lengthOfPayload);
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.write(payload);
    }

    @Override
    public int getLengthOfPayload() {
        return payload.length;
    }
}
