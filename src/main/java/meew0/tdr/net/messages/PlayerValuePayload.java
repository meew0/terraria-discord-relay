package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class PlayerValuePayload extends ClientToServerOnlyPayload {
    private final int playerId, value;

    public PlayerValuePayload(int playerId, int value) {
        this.playerId = playerId;
        this.value = value;
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.write(playerId);
        os.writeShort(value);
        os.writeShort(value);
    }

    @Override
    public int getLengthOfPayload() {
        return 5;
    }
}
