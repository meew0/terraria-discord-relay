package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class RequestEssentialTiles08 extends ClientToServerOnlyPayload {
    private final int spawnX, spawnY;

    public RequestEssentialTiles08(int spawnX, int spawnY) {
        this.spawnX = spawnX;
        this.spawnY = spawnY;
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.writeShort(spawnX);
        os.writeShort(spawnY);
    }

    @Override
    public int getLengthOfPayload() {
        return 4;
    }
}
