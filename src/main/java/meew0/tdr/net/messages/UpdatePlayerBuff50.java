package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class UpdatePlayerBuff50 extends ClientToServerOnlyPayload {
    private final int playerId;

    public UpdatePlayerBuff50(int playerId) {
        this.playerId = playerId;
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.write(playerId);
        os.writeShort(10); // Invisibility
        for (int i = 0; i < 21; i++) os.writeShort(0);
    }

    @Override
    public int getLengthOfPayload() {
        return 1 + 2 * 22;
    }
}
