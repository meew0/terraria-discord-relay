package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class SetUserSlot03 extends ServerToClientOnlyPayload {
    private int playerId;

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        playerId = is.readByte();
    }

    public int getPlayerId() {
        return playerId;
    }
}
