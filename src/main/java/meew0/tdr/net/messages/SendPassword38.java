package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataOutputStream;
import meew0.tdr.net.NetUtil;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class SendPassword38 extends ClientToServerOnlyPayload {
    private final String password;

    public SendPassword38(String password) {
        this.password = password;
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        NetUtil.writeString(os, password);
    }

    @Override
    public int getLengthOfPayload() {
        return NetUtil.getStringLength(password);
    }
}
