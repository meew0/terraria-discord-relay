package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;
import meew0.tdr.net.NetworkText;

import java.io.IOException;

/**
 * Created by meew0 on 20/05/2020.
 */
public class Disconnect02 extends ServerToClientOnlyPayload {
    private NetworkText reason;

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        reason = NetworkText.readFrom(is);
    }

    public NetworkText getReason() {
        return reason;
    }
}
