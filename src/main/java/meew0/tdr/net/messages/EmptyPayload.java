package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

/**
 * Created by meew0 on 19/05/2020.
 */
public class EmptyPayload implements IPayload {
    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) {
        if (lengthOfPayload != 0) throw new IllegalArgumentException("tried to read empty payload with data present");
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) {
    }

    @Override
    public int getLengthOfPayload() {
        return 0;
    }
}
