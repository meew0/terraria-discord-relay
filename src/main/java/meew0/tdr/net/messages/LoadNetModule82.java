package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import meew0.tdr.net.NetColour;
import meew0.tdr.net.NetUtil;
import meew0.tdr.net.NetworkText;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class LoadNetModule82 implements IPayload {
    public LoadNetModule82() {
    }

    private int moduleId;
    private byte[] payload;

    public LoadNetModule82(int moduleId, byte[] payload) {
        this.moduleId = moduleId;
        this.payload = payload;
    }

    public static LoadNetModule82 createSayChatCommand(String content) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            LittleEndianDataOutputStream os = new LittleEndianDataOutputStream(baos);
            NetUtil.writeString(os, "Say");
            NetUtil.writeString(os, content);
            os.close();
            return new LoadNetModule82(1, baos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        moduleId = is.readShort();
        payload = is.readNBytes(lengthOfPayload - 2);
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.writeShort(moduleId);
        os.write(payload);
    }

    @Override
    public int getLengthOfPayload() {
        return payload.length + 2;
    }

    public int getModuleId() {
        return moduleId;
    }

    public ChatMessage getPayloadAsChatMessage() {
        try {
            LittleEndianDataInputStream bais = new LittleEndianDataInputStream(new ByteArrayInputStream(payload));
            int authorId = bais.read();
            NetworkText text = NetworkText.readFrom(bais);
            NetColour colour = NetColour.readFrom(bais);
            bais.close();
            return new ChatMessage(authorId, text, colour);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static class ChatMessage {
        private final int authorId;
        private final NetworkText text;
        private final NetColour colour;

        public ChatMessage(int authorId, NetworkText text, NetColour colour) {
            this.authorId = authorId;
            this.text = text;
            this.colour = colour;
        }

        public int getAuthorId() {
            return authorId;
        }

        public NetworkText getText() {
            return text;
        }

        public NetColour getColour() {
            return colour;
        }

        @Override
        public String toString() {
            return "ChatMessage{" +
                    "authorId=" + authorId +
                    ", text=" + text +
                    ", colour=" + colour +
                    '}';
        }
    }
}
