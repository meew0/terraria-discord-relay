package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class SpawnPlayer12 extends ClientToServerOnlyPayload {
    private final int playerId, spawnX, spawnY;

    public SpawnPlayer12(int playerId, int spawnX, int spawnY) {
        this.playerId = playerId;
        this.spawnX = spawnX;
        this.spawnY = spawnY;
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.write(playerId);
        os.writeShort(spawnX);
        os.writeShort(spawnY);
        os.writeInt(0); // no respawn time
        os.write(1); // spawning into world
    }

    @Override
    public int getLengthOfPayload() {
        return 10;
    }
}
