package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataOutputStream;
import meew0.tdr.net.NetUtil;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class ConnectRequest01 extends ClientToServerOnlyPayload {
    private static final String VERSION = "Terraria226";

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        NetUtil.writeString(os, VERSION);
    }

    @Override
    public int getLengthOfPayload() {
        return NetUtil.getStringLength(VERSION);
    }
}
