package meew0.tdr.net.messages;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;

/**
 * Created by meew0 on 20/05/2020.
 */
public class UpdateItemOwner22 implements IPayload {
    private int itemId, playerId;

    public UpdateItemOwner22() {
    }

    public UpdateItemOwner22(int itemId, int playerId) {
        this.itemId = itemId;
        this.playerId = playerId;
    }

    @Override
    public void readFrom(LittleEndianDataInputStream is, int lengthOfPayload) throws IOException {
        itemId = is.readShort();
        playerId = is.read();
    }

    @Override
    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.writeShort(itemId);
        os.write(playerId);
    }

    @Override
    public int getLengthOfPayload() {
        return 3;
    }

    public int getItemId() {
        return itemId;
    }

    public int getPlayerId() {
        return playerId;
    }
}
