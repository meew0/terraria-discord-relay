package meew0.tdr.net;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;
import java.util.EnumSet;

/**
 * Created by meew0 on 20/05/2020.
 */
public class NetUtil {
    public static <E extends Enum<E>> void readEnumSet(LittleEndianDataInputStream is, EnumSet<E> set, E[] values) throws IOException {
        int val = is.read(), ordinal = 0;
        set.clear();
        do {
            if ((val & 1) == 1) set.add(values[ordinal]);
            ordinal++;
            val >>= 1;
        } while (val != 0);
    }

    public static <E extends Enum<E>> void writeEnumSet(LittleEndianDataOutputStream os, EnumSet<E> set) throws IOException {
        int val = 0;
        for (E e : set) {
            if (e.ordinal() > 7) throw new IllegalArgumentException("EnumSet must have 8 elements at most");
            val |= (1 << e.ordinal());
        }
        os.write(val);
    }

    public static String readString(LittleEndianDataInputStream is) throws IOException {
        int len = read7BitEncodedInt(is);
        byte[] bytes = is.readNBytes(len);
        return new String(bytes);
    }

    public static void writeString(LittleEndianDataOutputStream os, String str) throws IOException {
        byte[] bytes = str.getBytes();
        write7BitEncodedInt(os, bytes.length);
        os.write(bytes);
    }

    public static int getStringLength(String str) {
        byte[] bytes = str.getBytes();
        int lengthBytes = 0, value = bytes.length;
        do {
            value >>= 7;
            lengthBytes++;
        } while (value != 0);
        return lengthBytes + bytes.length;
    }

    public static int read7BitEncodedInt(LittleEndianDataInputStream is) throws IOException {
        int ret = 0, sevenLsb;
        do {
            sevenLsb = is.read();
            ret = (ret << 7) | (sevenLsb & 0b01111111);
        } while ((sevenLsb & 0b10000000) == 0b10000000);
        return ret;
    }

    public static void write7BitEncodedInt(LittleEndianDataOutputStream os, int value) throws IOException {
        int sevenLsb; // least significant bits of value
        do {
            sevenLsb = value & 0b01111111;
            value >>= 7;
            if (value != 0) sevenLsb |= 0b10000000;
            os.write(sevenLsb);
        } while (value != 0);
    }
}
