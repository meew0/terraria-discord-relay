package meew0.tdr.net;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import meew0.tdr.net.messages.*;

import java.io.IOException;

/**
 * Created by meew0 on 19/05/2020.
 */
public class Message {
    private final short type;
    private final IPayload payload;

    public Message(short type, IPayload payload) {
        this.type = type;
        this.payload = payload;
    }

    public short getType() {
        return type;
    }

    public IPayload getPayload() {
        return payload;
    }

    public static Message readFrom(LittleEndianDataInputStream is) throws IOException {
        int length = is.readUnsignedShort();
        byte type = is.readByte();
        IPayload payload = instantiatePayload(type);
        payload.readFrom(is, length - 3);
        return new Message(type, payload);
    }

    private static IPayload instantiatePayload(byte type) {
        switch (type) {
            case 2:
                return new Disconnect02();
            case 3:
                return new SetUserSlot03();
            case 4:
                return new PlayerInfo04();
            case 7:
                return new WorldInfo07();
            case 13:
                return new UpdatePlayer13();
            case 14:
                return new PlayerActive14();
            case 22:
                return new UpdateItemOwner22();
            case 37:
                return new RequestPassword37();
            case 49:
                return new CompleteConnectionAndSpawn49();
            case 82:
                return new LoadNetModule82();
            default:
                return new GenericPayload();
        }
    }

    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.writeShort(payload.getLengthOfPayload() + 3);
        os.writeByte(type);
        payload.writeTo(os);
    }

    @Override
    public String toString() {
        return "Message{" +
                "type=" + type +
                ", payload=" + payload +
                '}';
    }
}
