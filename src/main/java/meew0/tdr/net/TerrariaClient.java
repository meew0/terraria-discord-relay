package meew0.tdr.net;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;
import meew0.tdr.TDRBot;
import meew0.tdr.discord.TerrariaEmbedFormatter;
import meew0.tdr.net.messages.*;

import java.io.IOException;
import java.net.Socket;
import java.util.*;
import java.util.function.Supplier;

/**
 * Created by meew0 on 19/05/2020.
 */
public class TerrariaClient {
    private final Socket socket;
    private final LittleEndianDataInputStream is;
    private final LittleEndianDataOutputStream os;
    private final Supplier<String> passwordSupplier;
    private final String terrariaCharacterName;

    private final Queue<String> messagesToSend = new LinkedList<>();

    private int playerId;
    private WorldInfo07 worldInfo;

    private final HashMap<Integer, String> players = new HashMap<>();
    private final HashSet<Integer> activePlayers = new HashSet<>();

    private long lastUpdateMs;

    public TerrariaClient(String host, int port, Supplier<String> passwordSupplier, String terrariaCharacterName) throws IOException {
        this.terrariaCharacterName = terrariaCharacterName;
        socket = new Socket(host, port);

        is = new LittleEndianDataInputStream(socket.getInputStream());
        os = new LittleEndianDataOutputStream(socket.getOutputStream());

        this.passwordSupplier = passwordSupplier;
    }

    private void send(int id, IPayload payload) throws IOException {
        Message msg = new Message((byte) id, payload);
        msg.writeTo(os);
    }

    public void login() throws IOException {
        new Message((byte) 1, new ConnectRequest01()).writeTo(os);
        Message response = Message.readFrom(is);
        if (response.getPayload() instanceof RequestPassword37) {
            new Message((byte) 38, new SendPassword38(passwordSupplier.get())).writeTo(os);
            response = Message.readFrom(is);
        }

        if (response.getPayload() instanceof SetUserSlot03) {
            playerId = ((SetUserSlot03) response.getPayload()).getPlayerId();
        } else abortLogin(response);

        sendPlayerInfo();
        requestWorldInfo();

        do {
            response = Message.readFrom(is);
            handleMessage(response);
        } while (!(response.getPayload() instanceof WorldInfo07));
        worldInfo = (WorldInfo07) response.getPayload();

        requestTileData();

        do {
            response = Message.readFrom(is);
            handleMessage(response);
        } while (!(response.getPayload() instanceof CompleteConnectionAndSpawn49));

        spawnInWorld();
    }

    public void loop() throws IOException {
        while (socket.isConnected()) {
            Message msg = Message.readFrom(is);
            handleMessage(msg);

            while (!messagesToSend.isEmpty()) {
                send(82, LoadNetModule82.createSayChatCommand(messagesToSend.remove()));
            }

            // Keep our player alive at their current position
            if ((System.currentTimeMillis() - lastUpdateMs) >= 1000) {
                updateHealth();
                setPosition();
                lastUpdateMs = System.currentTimeMillis();
            }
        }
    }

    public void sendChat(String content) {
        messagesToSend.add(content);
    }

    private void handleMessage(Message msg) throws IOException {
        if (msg.getPayload() instanceof Disconnect02) {
            socket.close();
            throw new IOException("Disconnected with reason: " + ((Disconnect02) msg.getPayload()).getReason());
        }

        if (msg.getPayload() instanceof PlayerInfo04) {
            PlayerInfo04 playerInfo = (PlayerInfo04) msg.getPayload();
            players.put(playerInfo.getPlayerId(), playerInfo.getName());
            return;
        }

        if (msg.getPayload() instanceof PlayerActive14) {
            PlayerActive14 playerActive = (PlayerActive14) msg.getPayload();
            if (playerActive.isActive()) activePlayers.add(playerActive.getPlayerId());
            else activePlayers.remove(playerActive.getPlayerId());
            return;
        }

        if (msg.getPayload() instanceof UpdateItemOwner22) {
            // If Terraria tells us that we own an item, we need to disown it so other players can pick it up.
            UpdateItemOwner22 payload = (UpdateItemOwner22) msg.getPayload();
            if (payload.getPlayerId() == playerId) {
                UpdateItemOwner22 responsePayload = new UpdateItemOwner22(payload.getItemId(), 255);
                send(22, responsePayload);
            }
            return;
        }

        if (msg.getPayload() instanceof LoadNetModule82) {
            LoadNetModule82 payload = (LoadNetModule82) msg.getPayload();
            if (payload.getModuleId() == 1) {
                LoadNetModule82.ChatMessage chatMessage = payload.getPayloadAsChatMessage();
                if (chatMessage.getAuthorId() == playerId) return;
                TDRBot.sendDiscordMessage(TerrariaEmbedFormatter.terrariaMessageToEmbed(chatMessage,
                        players::get));
            }
        }
    }

    private void setPosition() throws IOException {
        send(13, new UpdatePlayer13(playerId, 0,
                EnumSet.noneOf(UpdatePlayer13.Control.class),
                EnumSet.noneOf(UpdatePlayer13.Pulley.class),
                EnumSet.noneOf(UpdatePlayer13.Misc.class),
                EnumSet.noneOf(UpdatePlayer13.SleepingInfo.class),
                16.f, 16.f));
    }

    private void updateHealth() throws IOException {
        send(16, new PlayerValuePayload(playerId, 199));
    }

    private void spawnInWorld() throws IOException {
        send(12, new SpawnPlayer12(playerId, worldInfo.getSpawnX(), worldInfo.getSpawnY()));
    }

    private void requestTileData() throws IOException {
        send(8, new RequestEssentialTiles08(worldInfo.getSpawnX(), worldInfo.getSpawnY()));
    }

    private void sendPlayerInfo() throws IOException {
        send(4, new PlayerInfo04(playerId, terrariaCharacterName));
        updateHealth();
        send(42, new PlayerValuePayload(playerId, 20)); // Mana
        send(50, new UpdatePlayerBuff50(playerId));
    }

    private void requestWorldInfo() throws IOException {
        send(6, new EmptyPayload());
    }

    private void abortLogin(Message response) {
        throw new RuntimeException("Aborting login: received " + response);
    }
}
