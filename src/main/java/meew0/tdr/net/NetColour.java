package meew0.tdr.net;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;

/**
 * Created by meew0 on 20/05/2020.
 */
public class NetColour {
    private final int r, g, b;

    public NetColour(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.write(r);
        os.write(g);
        os.write(b);
    }

    public static NetColour readFrom(LittleEndianDataInputStream is) throws IOException {
        int r = is.read();
        int g = is.read();
        int b = is.read();

        return new NetColour(r, g, b);
    }

    public int getRed() {
        return r;
    }

    public int getGreen() {
        return g;
    }

    public int getBlue() {
        return b;
    }

    @Override
    public String toString() {
        return "NetColour{" +
                "r=" + r +
                ", g=" + g +
                ", b=" + b +
                '}';
    }
}
