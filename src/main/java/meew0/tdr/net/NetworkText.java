package meew0.tdr.net;

import com.google.common.io.LittleEndianDataInputStream;
import com.google.common.io.LittleEndianDataOutputStream;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by meew0 on 20/05/2020.
 */
public class NetworkText {
    public enum Mode {
        Literal, Formattable, LocalizationKey
    }

    private final Mode mode;
    private final String text;
    private final NetworkText[] substitutionList;

    private NetworkText(Mode mode, String text, NetworkText[] substitutionList) {
        this.mode = mode;
        this.text = text;
        this.substitutionList = substitutionList;
    }

    public static NetworkText fromLiteral(String text) {
        return new NetworkText(Mode.Literal, text, new NetworkText[0]);
    }

    public static NetworkText readFrom(LittleEndianDataInputStream is) throws IOException {
        Mode mode = Mode.values()[is.read()];
        int stringLength = is.read();
        String text = new String(is.readNBytes(stringLength));

        NetworkText[] substitutionList;

        if (mode == Mode.Literal) {
            substitutionList = new NetworkText[0];
        } else {
            int substitutionListLength = is.read();
            substitutionList = new NetworkText[substitutionListLength];

            for (int i = 0; i < substitutionListLength; i++) {
                substitutionList[i] = NetworkText.readFrom(is);
            }
        }

        return new NetworkText(mode, text, substitutionList);
    }

    public void writeTo(LittleEndianDataOutputStream os) throws IOException {
        os.write(mode.ordinal());
        NetUtil.writeString(os, text);
        os.write(substitutionList.length);

        for (NetworkText substitution : substitutionList) {
            substitution.writeTo(os);
        }
    }

    public int getLength() {
        int length = 1 + NetUtil.getStringLength(text);
        if (mode != Mode.Literal)
            length += (1 + Arrays.stream(substitutionList).mapToInt(NetworkText::getLength).sum());
        return length;
    }

    public Mode getMode() {
        return mode;
    }

    public String getText() {
        return text;
    }

    public NetworkText[] getSubstitutionList() {
        return substitutionList;
    }

    @Override
    public String toString() {
        return "NetworkText{" +
                "mode=" + mode +
                ", text='" + text + '\'' +
                ", substitutionList=" + Arrays.toString(substitutionList) +
                '}';
    }
}