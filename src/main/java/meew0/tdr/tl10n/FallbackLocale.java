package meew0.tdr.tl10n;

import java.util.Arrays;

/**
 * Created by meew0 on 20/05/2020.
 */
public class FallbackLocale implements ILocale {
    private final ILocale[] locales;

    public FallbackLocale(ILocale... locales) {
        this.locales = locales;
    }

    @Override
    public String get(String key) {
        for (ILocale locale : locales) {
            String result = locale.get(key);
            if (result != null) return result;
        }
        return null;
    }

    @Override
    public String format(String key, String... values) {
        for (ILocale locale : locales) {
            String result = locale.format(key, values);
            if (result != null) return result;
        }
        return null;
    }

    @Override
    public String toString() {
        return "FallbackLocale{" +
                "locales=" + Arrays.toString(locales) +
                '}';
    }
}
