package meew0.tdr.tl10n;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by meew0 on 20/05/2020.
 */
public class LocaleLoader {
    public static final String ERROR_LOCALE_KEY = "_TDR.Message";

    private final Path basePath;
    @SuppressWarnings("deprecation")
    private final ObjectMapper objectMapper = new ObjectMapper().enable(JsonParser.Feature.ALLOW_TRAILING_COMMA);

    public LocaleLoader(Path basePath) {
        this.basePath = basePath;
    }

    private static Map<String, String> jsonToLocaleMap(JsonNode json, String prefix) {
        if (!json.isObject()) {
            throw new IllegalStateException("JSON file is not a JSON object");
        }

        ObjectNode object = (ObjectNode) json;
        HashMap<String, String> resultMap = new HashMap<>();

        object.fields().forEachRemaining(entry -> {
            if (entry.getValue().isObject()) {
                resultMap.putAll(jsonToLocaleMap(entry.getValue(), prefix + entry.getKey() + "."));
            } else {
                resultMap.put(prefix + entry.getKey(), entry.getValue().textValue());
            }
        });

        return resultMap;
    }

    private static ILocale getErrorLocale(String message) {
        System.err.println("ErrorLocale: " + message);

        return new FallbackLocale(new ILocale() {
            @Override
            public String get(String key) {
                if (key.equals(ERROR_LOCALE_KEY)) return message;
                return null;
            }

            @Override
            public String format(String key, String... values) {
                return null;
            }
        }, new EmptyLocale());
    }

    public ILocale loadOrEmpty(String localeIdentifier) {
        try {
            String glob = "**Content." + localeIdentifier + "*.json";
            PathMatcher pm = FileSystems.getDefault().getPathMatcher("glob:" + glob);
            List<Path> paths = Files.find(basePath, 1,
                    (path, basicFileAttributes) -> pm.matches(path))
                    .collect(Collectors.toList());

            if (paths.isEmpty()) return getErrorLocale("Could not find any locale files like '" + glob + "'");
            List<ILocale> localeFilesLoaded = new ArrayList<>();

            for (Path path : paths) {
                JsonNode element = objectMapper.readTree(path.toFile());
                localeFilesLoaded.add(new MapLocale(jsonToLocaleMap(element, ""), path));
            }

            localeFilesLoaded.add(new EmptyLocale());
            return new FallbackLocale(localeFilesLoaded.toArray(new ILocale[0]));
        } catch (Exception e) {
            return getErrorLocale("Exception occurred — " + e.getClass().getTypeName() + ": " + e.getMessage());
        }
    }

    private static class MapLocale implements ILocale {
        private final Map<String, String> keys;
        private final Path filePath;

        public MapLocale(Map<String, String> keys, Path filePath) {
            this.keys = keys;
            this.filePath = filePath;
        }

        @Override
        public String get(String key) {
            return keys.get(key);
        }

        @Override
        public String format(String key, String... values) {
            String formatBase = get(key);
            if (formatBase == null) return null;
            return LocaleUtil.format(formatBase, null, values);
        }

        @Override
        public String toString() {
            return "MapLocale{" +
                    "filePath=" + filePath +
                    '}';
        }
    }
}
