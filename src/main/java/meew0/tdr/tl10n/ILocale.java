package meew0.tdr.tl10n;

/**
 * Created by meew0 on 20/05/2020.
 */
public interface ILocale {
    String get(String key);

    String format(String key, String... values);
}
