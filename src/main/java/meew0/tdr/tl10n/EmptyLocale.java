package meew0.tdr.tl10n;

/**
 * Created by meew0 on 20/05/2020.
 */
public class EmptyLocale implements ILocale {
    @Override
    public String get(String key) {
        return key;
    }

    @Override
    public String format(String key, String... values) {
        return key + " [" + String.join(", ", values) + "]";
    }

    @Override
    public String toString() {
        return "EmptyLocale{}";
    }
}
