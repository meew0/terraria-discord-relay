package meew0.tdr.tl10n;

import meew0.tdr.net.NetworkText;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by meew0 on 20/05/2020.
 */
public class LocaleUtil {
    private static final Pattern FORMAT_NUMBERED_PATTERN = Pattern.compile("\\{(\\d+)}");
    private static final Pattern FORMAT_NAMED_PATTERN = Pattern.compile("\\{(\\w+)}");

    private static final Function<String, String> FORMAT_TRANSFORMER = input -> "**" + input + "**";

    public static String format(String base, Function<Integer, String> numberedParamProvider,
                                Function<String, String> namedParamProvider) {
        Matcher m = FORMAT_NUMBERED_PATTERN.matcher(base);
        StringBuffer sb = new StringBuffer();

        while (m.find()) {
            int index = Integer.parseInt(m.group(1));
            String result = numberedParamProvider.apply(index);
            m.appendReplacement(sb, Matcher.quoteReplacement(result == null ? m.group() : result));
        }
        m.appendTail(sb);

        if (namedParamProvider != null) {
            m = FORMAT_NAMED_PATTERN.matcher(sb);
            sb = new StringBuffer();

            while (m.find()) {
                String result = namedParamProvider.apply(m.group(1));
                m.appendReplacement(sb, Matcher.quoteReplacement(result == null ? m.group() : result));
            }
            m.appendTail(sb);
        }

        return sb.toString();
    }

    public static String format(String base, Function<String, String> namedParamProvider, String... params) {
        return format(base, index -> (index >= params.length ? null : params[index]), namedParamProvider);
    }

    public static String localiseNetworkText(ILocale locale, NetworkText text) {
        switch (text.getMode()) {
            case Literal:
                return text.getText();
            case Formattable:
                return format(text.getText(),
                        substitutionListToLocalisedNumberedParamProvider(locale, text.getSubstitutionList()),
                        null);
            case LocalizationKey:
                return locale.format(text.getText(), Arrays.stream(text.getSubstitutionList())
                        .map(networkText -> {
                            String localised = localiseNetworkText(locale, networkText);

                            // If we are replacing something with a literal text anywhere, make that literal text bold.
                            if (networkText.getMode() == NetworkText.Mode.Literal) {
                                localised = FORMAT_TRANSFORMER.apply(localised);
                            }
                            return localised;
                        }).toArray(String[]::new));
            default:
                throw new IllegalArgumentException("Invalid NetworkText mode: " + text.getMode());
        }
    }

    private static Function<Integer, String> substitutionListToLocalisedNumberedParamProvider(ILocale locale, NetworkText[] substitutionList) {
        List<String> list = Arrays.stream(substitutionList)
                .map(networkText -> localiseNetworkText(locale, networkText))
                .collect(Collectors.toUnmodifiableList());
        return index -> (index >= list.size() ? null : list.get(index));
    }
}
