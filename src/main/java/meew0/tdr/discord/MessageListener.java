package meew0.tdr.discord;

import meew0.tdr.TDRBot;
import meew0.tdr.net.TerrariaClient;
import meew0.tdr.tl10n.LocaleLoader;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.IMentionable;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.util.Arrays;

/**
 * Created by meew0 on 20/05/2020.
 */
public class MessageListener extends ListenerAdapter {
    private final TerrariaClient terraria;

    public MessageListener(TerrariaClient terraria) {
        this.terraria = terraria;
    }

    @Override
    public void onGuildMessageReceived(@Nonnull GuildMessageReceivedEvent event) {
        if (event.getMessage().isMentioned(event.getJDA().getSelfUser())) {
            if (Arrays.stream(TDRBot.config.getAdminUserIds()).anyMatch(id -> id.equals(event.getAuthor().getId()))) {
                String content = event.getMessage().getContentStripped();
                if (content.contains("set channel")) {
                    if (event.getMessage().getMentionedChannels().isEmpty()) {
                        event.getChannel().sendMessage("No channel mentioned.").queue();
                    } else {
                        GuildChannel channel = event.getMessage().getMentionedChannels().get(0);
                        TDRBot.config.setChannelId(channel.getId());
                        event.getChannel().sendMessage(new MessageBuilder()
                                .append("Now sending in ").append((IMentionable) channel).build()).queue();
                    }
                } else if (content.contains("set locale")) {
                    String[] splitContent = content.split(" ");
                    String localeId = splitContent[splitContent.length - 1];
                    event.getChannel().sendMessage("Setting locale to `" + localeId + "`").queue();
                    TDRBot.config.setLocale(localeId);
                    String error = TDRBot.config.getLocale().get(LocaleLoader.ERROR_LOCALE_KEY);
                    if (error != null && !error.equals(LocaleLoader.ERROR_LOCALE_KEY)) {
                        event.getChannel().sendMessage("Error while setting locale: `" +
                                TDRBot.config.getLocale().get(LocaleLoader.ERROR_LOCALE_KEY) +
                                "`.\n Falling back to empty locale").queue();
                    }
                }
            }
        }

        if (event.getMessage().getChannel().equals(TDRBot.config.getChannel()) &
                !event.getMessage().getAuthor().equals(event.getJDA().getSelfUser())) {
            String effectiveName = event.getMember() == null
                    ? event.getAuthor().getName()
                    : event.getMember().getEffectiveName();
            terraria.sendChat("[" + effectiveName + "] " + event.getMessage().getContentStripped());
        }
    }
}
