package meew0.tdr.discord;

import meew0.tdr.TDRBot;
import meew0.tdr.net.messages.LoadNetModule82;
import meew0.tdr.tl10n.LocaleUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.awt.*;
import java.util.function.Function;

/**
 * Created by meew0 on 20/05/2020.
 */
public class TerrariaEmbedFormatter {
    public static MessageEmbed terrariaMessageToEmbed(LoadNetModule82.ChatMessage terrariaMessage,
                                                      Function<Integer, String> playerNameProvider) {
        EmbedBuilder eb = new EmbedBuilder();

        if (terrariaMessage.getAuthorId() != 255) {
            eb.setAuthor(playerNameProvider.apply(terrariaMessage.getAuthorId()));
        }

        eb.setDescription(LocaleUtil.localiseNetworkText(TDRBot.config.getLocale(), terrariaMessage.getText()));
        eb.setColor(new Color(terrariaMessage.getColour().getRed(), terrariaMessage.getColour().getGreen(),
                terrariaMessage.getColour().getBlue()));

        return eb.build();
    }
}
