package meew0.tdr;

import com.google.common.base.Suppliers;
import meew0.tdr.discord.MessageListener;
import meew0.tdr.net.TerrariaClient;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by meew0 on 19/05/2020.
 */
public class TDRBot {
    public static Config config;

    public static void sendDiscordMessage(String message) {
        config.getChannel().sendMessage(message).queue();
    }

    public static void sendDiscordMessage(MessageEmbed embed) {
        config.getChannel().sendMessage(embed).queue();
    }

    public static void main(String[] args) throws IOException, LoginException, InterruptedException {
        JDA jda = JDABuilder.create(Files.readString(Path.of("token")), GatewayIntent.GUILD_MESSAGES)
                .setMemberCachePolicy(MemberCachePolicy.NONE)
                .disableCache(CacheFlag.ACTIVITY, CacheFlag.VOICE_STATE, CacheFlag.EMOTE, CacheFlag.CLIENT_STATUS)
                .build();
        jda.awaitReady();

        config = Config.read(Path.of("config.json"), jda);

        TerrariaClient client = new TerrariaClient(
                config.getServerHost(),
                config.getServerPort(),
                Suppliers.ofInstance(config.getServerPassword()), "Discord");

        client.login();
        System.out.println("Successfully logged into Terraria.");
        jda.addEventListener(new MessageListener(client));

        client.loop();
        System.exit(0);
    }
}
