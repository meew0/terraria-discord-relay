package meew0.tdr;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import meew0.tdr.tl10n.ILocale;
import meew0.tdr.tl10n.LocaleLoader;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.TextChannel;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by meew0 on 20/05/2020.
 */
public class Config {
    public static final ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    private final String l10nDataBasePath;
    private Path configFilePath;
    private LocaleLoader localeLoader;
    private JDA jda;
    private String locale;
    private ILocale _locale;
    private String channelId;
    private TextChannel channel;
    @JsonProperty("adminUserIds")
    private final String[] adminUserIds;

    public static Config read(Path configFilePath, JDA jda) throws IOException {
        Config ret = mapper.readValue(Files.newBufferedReader(configFilePath), Config.class);
        ret.configFilePath = configFilePath;
        ret.localeLoader = new LocaleLoader(Path.of(ret.l10nDataBasePath));
        ret.jda = jda;

        ret.updateLocale();
        ret.updateChannel();

        return ret;
    }

    private final String serverHost;
    private final String serverPassword;
    private final String characterName;
    private final int serverPort;

    private Config(@JsonProperty("l10nDataBasePath") String l10nDataBasePath,
                   @JsonProperty("locale") String localeId,
                   @JsonProperty("channelId") String channelId,
                   @JsonProperty("adminUserIds") String[] adminUserIds,
                   @JsonProperty("serverHost") String serverHost,
                   @JsonProperty("serverPort") int serverPort,
                   @JsonProperty("serverPassword") String serverPassword,
                   @JsonProperty("characterName") String characterName) {
        this.l10nDataBasePath = l10nDataBasePath;
        this.locale = localeId;
        this.channelId = channelId;
        this.adminUserIds = adminUserIds;
        this.serverHost = serverHost;
        this.serverPort = serverPort;
        this.serverPassword = serverPassword;
        this.characterName = characterName;
    }

    @JsonProperty("serverHost")
    public String getServerHost() {
        return serverHost;
    }

    @JsonProperty("serverPassword")
    public String getServerPassword() {
        return serverPassword;
    }

    @JsonProperty("serverPort")
    public int getServerPort() {
        return serverPort;
    }

    @JsonProperty("characterName")
    public String getCharacterName() {
        return characterName;
    }

    @JsonProperty("l10nDataBasePath")
    public String getL10nDataBasePath() {
        return l10nDataBasePath;
    }

    public String[] getAdminUserIds() {
        return adminUserIds;
    }

    @JsonProperty("locale")
    public String getLocaleId() {
        return locale;
    }

    @JsonIgnore
    public ILocale getLocale() {
        return _locale;
    }

    @JsonIgnore
    public void setLocale(String locale) {
        this.locale = locale;
        updateLocale();
        write();
    }

    private void updateLocale() {
        _locale = localeLoader.loadOrEmpty(locale);
    }

    @JsonProperty("channelId")
    public String getChannelId() {
        return channelId;
    }

    @JsonIgnore
    public void setChannelId(String channelId) {
        this.channelId = channelId;
        updateChannel();
        write();
    }

    @JsonIgnore
    public TextChannel getChannel() {
        return channel;
    }

    private void updateChannel() {
        channel = jda.getTextChannelById(channelId);
    }

    private void write() {
        try {
            mapper.writeValue(Files.newBufferedWriter(configFilePath), this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
