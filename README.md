# Terraria ↔ Discord Relay

This project does exactly what its name suggests: it relays information between a Terraria (1.4) server and a Discord channel.

It accomplishes this by connecting to the server using an invisible, invincible player that does nothing but read and write to chat. (The Discord side is just a regular Discord bot) Compared to alternative approaches like interfacing with the standard input and output of the server process, this approach isn't very clean — for example, I can't prevent the fake player from showing up on the map or member lists —, but it is far more extensible. It also requires zero cooperation from the server.

In contrast to most other game servers, Terraria trusts and relies on the client to a large extent. This is good, because it allows us to have an invincible fake player in the first place, but it also means each client has to do a bunch of stuff to keep the game running properly. I tried to implement most of the things I noticed (like disowning items Terraria thinks the fake player owns, so other people can pick them up), but **if you notice any strange game bugs that occur only while the fake player is online, please report them.**

As this project implements a core fraction of the Terraria protocol, it might also be useful as a base for fully custom client/server implementations.

## Using this project

Use gradle to build the project. Then, create a plain text file called `token` in the root directory that contains the token for the Discord bot. Copy the `config.example.json` file to `config.json` and edit the configuration as you desire. Finally, run the program. If it works, you should see a new player joining your Terraria server, as well as some startup messages in the Discord server you configured.